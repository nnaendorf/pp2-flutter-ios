import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vater_app_flutter/authentication/user.dart';
import 'package:vater_app_flutter/screens/home_screen.dart';
import 'package:vater_app_flutter/widgets/app_drawer.dart';
import 'package:vater_app_flutter/widgets/licenses/image_preview.dart';
import 'package:async/async.dart';
import 'dart:convert';


class LicensesScreen extends StatefulWidget {
  static const routeName = '/licenses-screen';
  final ImagePreview imagePreview = new ImagePreview();

  @override
  _LicensesScreenState createState() => _LicensesScreenState();
}

class _LicensesScreenState extends State<LicensesScreen> {

  void uploadImages(File frontside, File backside, String session) async {

    if (frontside == null || backside == null) {
      _showAlert("Upload Fehlgeschlagen!", "Bitte nehmen Sie zwei Bilder Ihres Führerscheins auf!");
    } else {
      var uri = Uri.parse("http://212.227.10.211:8080/pp-app-server/driverlicense");
      var request = new http.MultipartRequest("POST", uri);

      var streamFront = new http.ByteStream(DelegatingStream.typed(frontside.openRead()));
      var lengthFront = await frontside.length();
      var frontsideMultipartFile = new http.MultipartFile('frontside', streamFront, lengthFront,
        filename: "frontside");

      var streamBack = new http.ByteStream(DelegatingStream.typed(frontside.openRead()));
      var lengthBack = await frontside.length();
      var backsideMultipartFile = new http.MultipartFile('backside', streamBack, lengthBack,
        filename: "backside");

      request.files.add(frontsideMultipartFile);
      request.files.add(backsideMultipartFile);
      request.headers['cookie'] = session; 

      // send
      var response = await request.send();
      if (response.statusCode != 200) {
        _showAlert("Einreichen Fehlgeschlagen!", "Ein Fehler ist aufgetreten! Statuscode: " + response.statusCode.toString());
      }

      // listen for response
      response.stream.transform(utf8.decoder).listen((value) {
        print(value);

      // navigate back
      Navigator.of(context).pushNamed(
        HomeScreen.routeName,
      );
      });
    }
  }

  void _showAlert(String title, String body) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(title),
          content: new Text(body),
          actions: <Widget>[
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<User>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Führerschein"),
        actions: <Widget>[
          BackButton(),
        ],
      ),
      drawer: AppDrawer(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: <Widget>[
                    widget.imagePreview,
                  ],
                ),
              ),
            ),
          ),
          RaisedButton.icon(
            color: Colors.green,
            textColor: Colors.white,
            icon: Icon(Icons.file_upload),
            label: Text("EINREICHEN"),
            onPressed: () {
              uploadImages(widget.imagePreview.frontside, widget.imagePreview.backside, user.session);
            },
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          ),
        ],
      ),
    );
  }
}