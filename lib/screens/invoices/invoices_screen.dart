import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:vater_app_flutter/authentication/user.dart';
import 'package:vater_app_flutter/screens/home_screen.dart';
import 'package:vater_app_flutter/widgets/app_drawer.dart';
import 'package:vater_app_flutter/widgets/invoices/invoice.dart';
import 'package:http/http.dart' as http;
import 'package:async/async.dart';
import 'dart:convert';

class InvoicesScreen extends StatefulWidget {
  static const routeName = '/invoices-screen';

  @override
  _InvoicesScreenState createState() => _InvoicesScreenState();
}

class _InvoicesScreenState extends State<InvoicesScreen> {
  var _invoice = Invoice(
    title: '',
    description: '',
    price: 0,
    image: null,
  );
  final _descFocusNode = FocusNode();
  final _priceFocusNode = FocusNode();
  final GlobalKey<FormState> _invoiceKey = GlobalKey();

  Future<void>_takePicture(ImageSource source) async {
    final imageFile = await ImagePicker.pickImage(
      source: source,
      maxWidth: 600,
      maxHeight: 600,
    );
    setState(() {
      _invoice.image = imageFile;
    });
  }

  void _uploadForm(User user) {
    if (_invoice.image == null) {
      _showAlert("Upload Fehlgeschlagen!", "Bitte wählen Sie ein valides Bild zum Upload aus!");
    } else if (_invoiceKey.currentState.validate()) {
      _invoiceKey.currentState.save();
      _uploadImage(_invoice.image, _invoice.title, _invoice.description, _invoice.price, user.session);
      Navigator.of(context).pushNamed(HomeScreen.routeName);
    }
  }

  void _uploadImage(File image, String title, String description, double price, String session) async {
    var uri = Uri.parse("http://212.227.10.211:8080/pp-app-server/invoices");
    var request = new http.MultipartRequest("POST", uri);

    var stream = new http.ByteStream(DelegatingStream.typed(image.openRead()));
    var length = await image.length();
    var multipartFile = new http.MultipartFile('invoice', stream, length,
        filename: "invoice");

    request.files.add(multipartFile);
    request.fields['title'] = title;
    request.fields['description'] = description;
    request.fields['price'] = price.toString();
    request.headers['cookie'] = session; 

    // send
    var response = await request.send();
    if (response.statusCode != 200) {
      _showAlert("Einreichen Fehlgeschlagen", "Etwas ist schiefgelaufen! Statuscode: " + response.statusCode.toString());
    }

    // listen for response
    response.stream.transform(utf8.decoder).listen((value) {
      print(value);
    });
  }

  void _showAlert(String title, String body) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(title),
          content: new Text(body),
          actions: <Widget>[
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    var user = Provider.of<User>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Belege"),
        actions: <Widget>[
          BackButton(),
        ],
      ),
      drawer: AppDrawer(),
      body: Form(
        key: _invoiceKey,
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              Card(
                margin: EdgeInsets.only(
                  top: deviceSize.height/75,
                  left: deviceSize.width/60,
                  right: deviceSize.width/60,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 8,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 15),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      TextFormField(
                        decoration: InputDecoration(labelText: "Titel"),
                        textInputAction: TextInputAction.next,
                        onFieldSubmitted: (value) {
                          FocusScope.of(context).requestFocus(_descFocusNode);
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Bitte geben Sie einen Titel ein!";
                          } else {
                            return null;
                          }
                        },
                        onSaved: (value) {
                          _invoice.title = value;
                        }
                      ),

                      TextFormField(
                        decoration: InputDecoration(
                          labelText: 'Kurze Beschreibung',
                        ),
                        maxLines: 3,
                        focusNode: _descFocusNode,
                        keyboardType: TextInputType.multiline,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Bitte geben Sie eine Beschreibung ein!';
                          } else if (value.length > 180 || value.length < 10) {
                            return 'Beschreibung muss aus 10 bis 180 Zeichen bestehen!';
                          }
                          return null;
                        },
                        onSaved: (value) {
                          _invoice.description = value;
                        }
                      ),

                      TextFormField(
                        decoration: InputDecoration(
                          labelText: 'Preis',
                        ),
                        focusNode: _priceFocusNode,
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Bitte geben Sie einen Preis ein!';
                          }
                          if (double.tryParse(value) == null) {
                            return 'Bitte geben Sie einen validen Preis ein!';
                          }
                          return null;
                        },
                        onSaved: (value) {
                          _invoice.price = double.parse(value);
                        }
                      ),    
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.all(10),
                            child: FlatButton(
                              padding: EdgeInsets.all(12),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0)
                              ),
                              textColor: Colors.white,
                              color: Colors.green,
                              child: Text("ABSCHICKEN",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold
                                ),
                              ),
                              onPressed: () {
                                _uploadForm(user);
                              },
                            ),
                          ),
                        ],
                      ),
                    ]
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 30, bottom: 80),
                width: deviceSize.width*0.9,
                height: deviceSize.width*0.9,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1,
                    color: Theme.of(context).primaryColor,
                    style: BorderStyle.solid,
                  ),
                ),
                child: _invoice.image != null ?
                  Image.file(_invoice.image,
                    fit: BoxFit.cover,
                    width: double.infinity,
                  ) :
                  Text("Beleg"),
                alignment: Alignment.center,
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: SpeedDial(
        animatedIcon: AnimatedIcons.menu_close,
        children: [
          SpeedDialChild(
            child: Icon(Icons.camera),
            label: "Kamera",
            backgroundColor: Colors.lightBlueAccent,
            onTap: () => _takePicture(ImageSource.camera),
          ),
          SpeedDialChild(
            child: Icon(Icons.photo_album),
            label: "Galerie",
            backgroundColor: Colors.lightBlueAccent,
            onTap: () => _takePicture(ImageSource.gallery),
          ),
        ],
      ),
    );
  }
}