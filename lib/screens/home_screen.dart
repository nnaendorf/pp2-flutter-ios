import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vater_app_flutter/authentication/user.dart';
import 'package:vater_app_flutter/screens/invoices/invoices_screen.dart';
import 'package:vater_app_flutter/screens/licenses/licenses_screen.dart';
import 'package:vater_app_flutter/screens/reminders/reminders_screen.dart';
import 'package:vater_app_flutter/widgets/app_drawer.dart';
import 'package:vater_app_flutter/widgets/list_item.dart';
import 'package:vater_app_flutter/widgets/reminders/reminder.dart';
import 'package:http/http.dart' as http;

class HomeScreen extends StatefulWidget {
  static const routeName = "/home";

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Reminder reminderList;
  bool _registered = false;
  String _message = '';
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    getMessage();
    super.initState();
  }

  void getMessage(){
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
      print('on message $message');
      setState(() => _message = message["notification"]["title"]);
      showReminder(message);
    }, onResume: (Map<String, dynamic> message) async {
      print('on resume $message');
      setState(() => _message = message["notification"]["title"]);
    }, onLaunch: (Map<String, dynamic> message) async {
      print('on launch $message');
      setState(() => _message = message["notification"]["title"]);
    });
  }

  void _register(String token, String session) async {
    await http.post('http://212.227.10.211:8080/pp-app-server/register',
      body: {
        'token': token,
      },
      headers: {
        HttpHeaders.cookieHeader: session,
      } 
    );
  }

  void _unregister(String token, String session) async {
    http.post('http://212.227.10.211:8080/pp-app-server/unregister',
      body: {
        'token': token,
      },
      headers: {
        HttpHeaders.cookieHeader: session,
      } 
    );
  }

  Future showReminder(Map<String, dynamic> message) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(message['notification']['title']),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(message['notification']['body']),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget buildRaisedButton(
      Color startingColor, Color endingColor, String text, Function function) {
    return RaisedButton(
      elevation: 8,
      shape:
          RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
      child: Ink(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          gradient: LinearGradient(
            colors: <Color>[
              startingColor,
              endingColor,
            ],
          ),
        ),
        child: Container(
          constraints: const BoxConstraints(maxWidth: 250.0, maxHeight: 45.0),
          alignment: Alignment.center,
          child: Text(
            text,
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
            ),
          ),
        ),
      ),
      onPressed: function,
      padding: const EdgeInsets.all(0.0),
    );
  }

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<User>(context);
    var device = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Übersicht"),
        actions: <Widget>[
          PopupMenuButton(
            itemBuilder: (context) => [
              PopupMenuItem(
                value: 1,
                child: ListTile(
                  leading: Icon(Icons.refresh),
                  title: const Text("Aktualisieren"),
                  onTap: () => setState(() {}),
                ),
              ),
              PopupMenuItem(
                value: 2,
                child: StatefulBuilder(
                  builder: (BuildContext context, StateSetter setState) {
                    return Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Expanded(
                          child: SizedBox(
                            height: 50,
                            child: ListTile(
                              leading: Icon(_registered ? Icons.check_box : Icons.check_box_outline_blank),
                              title: const Text("Push empfangen"),
                              onTap: () => setState(() {
                                _registered = !_registered;
                                _firebaseMessaging.getToken().then((token) {
                                  if (_registered == true) {
                                    _register(token, user.session);
                                  } else {
                                    _unregister(token, user.session);
                                  }
                                });
                              }),
                            ),
                          ),
                        ),
                      ],
                    );
                  }
                ),
              ),
            ],
          ),
        ],
      ),
      drawer: AppDrawer(),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                "Anstehende Ereignisse:",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  color: Colors.grey[600],
                ),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Flexible(
                    fit: FlexFit.loose,
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border(top: BorderSide(), bottom: BorderSide()),
                      ),
                      constraints: BoxConstraints(maxHeight: device.height/2.5),
                      child: FutureBuilder(
                        future: Future.wait([Provider.of<Reminder>(context).fetchReminders(context)]),
                        builder: (context, snapshot) {
                          if (snapshot.hasData && snapshot.hasData != null) {
                            reminderList = Provider.of<Reminder>(context);
                            var filteredList = reminderList.reminders.where((reminder) {
                              return DateTime.parse(reminder.startTime).isAfter(DateTime.now());
                            }).toList();
                            return Scrollbar(
                              child: filteredList.length != 0 ? 
                              ListView.builder(
                                shrinkWrap: true,
                                itemCount: filteredList.length,
                                itemBuilder: (context, index) {
                                  return ListItem(
                                    new ReminderObject(
                                      id: filteredList[index].id,
                                      user: filteredList[index].user,
                                      title: filteredList[index].title, 
                                      description: filteredList[index].description,
                                      startTime: filteredList[index].startTime, 
                                      endTime: filteredList[index].endTime, 
                                      allDay: filteredList[index].allDay,
                                    ),
                                    index == filteredList.length-1 ? true : false, 
                                  );            
                                }
                              ) : Text(
                                "Keine",
                                style: TextStyle(
                                  color: Colors.grey[600],
                                  fontSize: 20,
                                ),
                              ),
                            );
                          } else {
                            return Container(height: 0, width: 0);
                          }
                        }
                      ),
                    ),
                  ),
                  buildRaisedButton(
                    Colors.blue,
                    Colors.blueAccent,
                    "Belege",
                    () {
                      Navigator.of(context).pushNamed(
                        InvoicesScreen.routeName,
                      );
                    }
                  ),
                  buildRaisedButton(
                    Colors.blue,
                    Colors.blueAccent,
                    "Führerschein",
                    () {
                      Navigator.of(context).pushNamed(
                        LicensesScreen.routeName,
                      );
                    }
                  ),
                  buildRaisedButton(
                    Colors.blue,
                    Colors.blueAccent,
                    "Erinnerungen",
                    () {
                      Navigator.of(context).pushNamed(
                        RemindersScreen.routeName,
                      );
                    }
                  ),
                  buildRaisedButton(
                    Colors.red,
                    Colors.deepOrange,
                    "Logout",
                    () {
                      Navigator.of(context).pushReplacementNamed(
                        "/",
                      );
                    }
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
