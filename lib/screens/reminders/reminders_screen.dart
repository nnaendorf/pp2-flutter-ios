import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:vater_app_flutter/widgets/app_drawer.dart';
import 'package:vater_app_flutter/widgets/reminders/new_reminder.dart';
import 'package:vater_app_flutter/widgets/reminders/reminder.dart';

class RemindersScreen extends StatefulWidget {
  static const routeName = '/reminders-screen';

  @override
  _RemindersScreenState createState() => _RemindersScreenState();
}

class _RemindersScreenState extends State<RemindersScreen> {
  CalendarController _calendarController = CalendarController();
  Reminder _reminder = Reminder();
  Map<DateTime, List> _events = Map();
  List _selectedEvents = [];

  @override
  Future<void> didChangeDependencies() async {
    super.didChangeDependencies();
    updateEvents();
  }

  Future<void> updateEvents() async {
    var waitingForEvents = await _reminder.fetchReminders(context);
    print(waitingForEvents.toString());
    setState(() {
      _events = waitingForEvents;
    });
  }

  void _startAddNewTransaction(BuildContext context) async {
    final response = await showModalBottomSheet(context: context, builder: (builder) {
      return NewReminder();
    });
    if (response) {
      updateEvents();
    }
  }

  void _onDaySelected(DateTime date, List events) {
    setState(() {
      _selectedEvents = events;
    });
  }

  Widget _buildEventList() {
    return ListView(
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      children: _selectedEvents
        .map((event) => Container(
          decoration: BoxDecoration(
          border: Border.all(width: 0.8),
          borderRadius: BorderRadius.circular(12.0),
        ),
        margin:
          const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
        child: ListTile(
        title: Text(event.toString()),
        onTap: () {},
      ),
    )
    ).toList()
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Erinnerungen"),
        actions: <Widget>[
          BackButton(),
        ],
      ),
      drawer: AppDrawer(),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TableCalendar(
              events: _events,
              locale: 'de_GR',
              onDaySelected: (date, events) {
                _onDaySelected(date, events);
              },
              startingDayOfWeek: StartingDayOfWeek.monday,
              calendarController: _calendarController,
              headerStyle: HeaderStyle(
                centerHeaderTitle: true,
                formatButtonShowsNext: false,
                formatButtonDecoration: BoxDecoration(
                  color: Colors.orangeAccent,
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
              calendarStyle: CalendarStyle(
                todayColor: Colors.green,
                selectedColor: Theme.of(context).primaryColor,
                todayStyle: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),
            const SizedBox(height: 8),
            const SizedBox(height: 8),
            _buildEventList(),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => _startAddNewTransaction(context),
      ),
    );
  }
}