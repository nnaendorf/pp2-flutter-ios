import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:vater_app_flutter/authentication/auth_screen.dart';
import 'package:vater_app_flutter/authentication/user.dart';
import 'package:vater_app_flutter/screens/home_screen.dart';
import 'package:vater_app_flutter/screens/invoices/invoices_screen.dart';
import 'package:vater_app_flutter/screens/licenses/licenses_screen.dart';
import 'package:vater_app_flutter/screens/reminders/reminders_screen.dart';
import 'package:vater_app_flutter/widgets/reminders/reminder.dart';

import 'authentication/auth_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: User()),
        ChangeNotifierProvider.value(value: Reminder()),
      ],
      child: MaterialApp(
        builder: (context, child) =>
          MediaQuery(data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true), child: child),
        title: 'Vater App',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: AuthScreen(),
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('de', 'DE'),
          const Locale('en', 'US'),
        ],
        routes: {
          HomeScreen.routeName: (context) => HomeScreen(),
          InvoicesScreen.routeName: (context) => InvoicesScreen(),
          LicensesScreen.routeName: (context) => LicensesScreen(),
          RemindersScreen.routeName: (context) => RemindersScreen(),
        },
      ),
    );
  }
}