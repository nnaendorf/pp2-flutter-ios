import 'package:flutter/material.dart';
import 'package:vater_app_flutter/screens/invoices/invoices_screen.dart';
import 'package:vater_app_flutter/screens/licenses/licenses_screen.dart';
import 'package:vater_app_flutter/screens/reminders/reminders_screen.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              AppBar(
                title: Text('Vater App'),
                automaticallyImplyLeading: false,
              ),
              Divider(),
              ListTile(
                leading: Icon(Icons.person),
                title: Text('Übersicht'),
                onTap: () {
                  Navigator.of(context).pushReplacementNamed('/home');
                },
              ),
              Divider(),
              ListTile(
                leading: Icon(Icons.payment),
                title: Text('Belege'),
                onTap: () {
                  Navigator.of(context).pushReplacementNamed(InvoicesScreen.routeName);
                },
              ),
              Divider(),
              ListTile(
                leading: Icon(Icons.time_to_leave),
                title: Text('Führerschein'),
                onTap: () {
                  Navigator.of(context).pushReplacementNamed(LicensesScreen.routeName);
                },
              ),
              Divider(),
              ListTile(
                leading: Icon(Icons.schedule),
                title: Text('Erinnerungen'),
                onTap: () {
                  Navigator.of(context).pushReplacementNamed(RemindersScreen.routeName);
                },
              ),
              Divider(),
              ListTile(
                leading: Icon(Icons.arrow_back),
                title: Text('Logout'),
                onTap: () {
                  Navigator.of(context).pushReplacementNamed('/');
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}