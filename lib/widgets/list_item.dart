import 'package:flutter/material.dart';
import 'package:vater_app_flutter/widgets/reminders/reminder.dart';
import 'package:intl/intl.dart';
import 'package:getflutter/getflutter.dart';

class ListItem extends StatelessWidget {
  ReminderObject _reminder;
  bool _isLast;

  ListItem(ReminderObject reminder, bool _isLast) {
    _reminder = reminder;
    this._isLast = _isLast;
  }

  String stringBuilder() {
    DateTime start = DateTime.parse(_reminder.startTime);
    DateTime end = DateTime.parse(_reminder.endTime);
    DateTime ectStart = new DateTime(start.year, start.month, start.day, start.hour+2, start.minute);
    DateTime ectEnd = new DateTime(end.year, end.month, end.day, end.hour+2, end.minute);
    
    if (!_reminder.allDay) {
      return DateFormat('dd.MM.yyyy: HH:mm').format(ectStart) + ' Uhr - ' + DateFormat('HH:mm').format(ectEnd) + ' Uhr';
    } else {
      return DateFormat('dd.MM.yyyy:').format(ectStart) + ' Ganztägig';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(bottom: _isLast ? BorderSide.none : BorderSide()),
      ),
      padding: EdgeInsets.all(8),
      child: ListTile(
        leading: Container(
          child: GFAvatar(
            backgroundColor: Theme.of(context).primaryColor,
            shape: GFAvatarShape.standard,
            size: GFSize.LARGE,
            child: Center(
              child: Text(
                _reminder.title,
                overflow: TextOverflow.fade,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 13,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
        title: Text(
          stringBuilder(),
        ),
        subtitle: Text(
          _reminder.description,
        ),
      ),
    );
  }
}