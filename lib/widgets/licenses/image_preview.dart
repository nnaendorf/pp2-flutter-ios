import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ImagePreview extends StatefulWidget {
  File _frontside;
  File _backside;
  get frontside => _frontside;
  get backside => _backside;

  @override
  _State createState() => _State();
}

class _State extends State<ImagePreview> {

  Future<void>_takePicture(String side) async {
    final imageFile = await ImagePicker.pickImage(
      source: ImageSource.camera,
      maxWidth: 600,
      maxHeight: 600,
    );
    setState(() {
      if (side == "frontside") {
        widget._frontside = imageFile;
      } else {
        widget._backside = imageFile;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        GestureDetector(
          onTap: () => _takePicture("frontside"),
          child: Container(
            width: deviceSize.width*0.75,
            height: deviceSize.width*0.75,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1,
                color: Theme.of(context).primaryColor,
                style: BorderStyle.solid,
              ),
            ),
            child: widget._frontside != null ?
              Image.file(widget._frontside,
                fit: BoxFit.cover,
                width: double.infinity,
              ) :
              Text("Vorderseite"),
            alignment: Alignment.center,
          ),
        ),
        SizedBox(height: deviceSize.height/20),
        GestureDetector(
          onTap: () => _takePicture("backside"),
          child: Container(
            width: deviceSize.width*0.75,
            height: deviceSize.width*0.75,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1,
                color: Theme.of(context).primaryColor,
                style: BorderStyle.solid,
              ),
            ),
            child: widget._backside != null ?
            Image.file(widget._backside,
              fit: BoxFit.cover,
              width: double.infinity,
            ) :
            Text("Rückseite"),
            alignment: Alignment.center,
          ),
        ),
      ],
    );
  }
}