import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:vater_app_flutter/authentication/user.dart';
import 'package:vater_app_flutter/widgets/reminders/reminder.dart';

class NewReminder extends StatefulWidget {

  @override
  _NewReminderState createState() => _NewReminderState();
}

class _NewReminderState extends State<NewReminder> {
  final _descFocusNode = FocusNode();
  final GlobalKey<FormState> _reminderKey = GlobalKey();
  final _titleController = TextEditingController();
  final _descController = TextEditingController();
  final Reminder _reminder = Reminder();
  
  DateTime _selectedDate = DateTime.now();
  TimeOfDay _selectedStartTime = TimeOfDay.now();
  TimeOfDay _selectedEndTime = TimeOfDay.now();
  String _enteredTitle;
  String _enteredDesc;
  String _startDateTime;
  String _endDateTime;
  bool _isAllday = false;

  _onSubmit(String session) {
    if (_reminderKey.currentState.validate()) {
      _reminderKey.currentState.save();
      _startDateTime = stringDateTimeBuilder(_selectedDate, _selectedStartTime);
      _endDateTime = stringDateTimeBuilder(_selectedDate, _selectedEndTime);
      _enteredTitle = _titleController.text;
      _enteredDesc = _descController.text;
      _reminder.addReminder(_enteredTitle, _enteredDesc, _startDateTime, _endDateTime, _isAllday, session)
      .then((_) => Navigator.pop(context, true));
    }
  }

  String stringDateTimeBuilder(DateTime selectedDate, TimeOfDay selectedTime) {    
    if (selectedTime.minute < 10 && selectedTime.hour < 10) {
      return '${selectedDate.toString().substring(0, 10)}T0${selectedTime.hour}:0${selectedTime.minute}:00.000+02:00';
    } else if (selectedTime.hour < 10) {
      return '${selectedDate.toString().substring(0, 10)}T0${selectedTime.hour}:${selectedTime.minute}:00.000+02:00';
    } else if (selectedTime.minute < 10) {
      return '${selectedDate.toString().substring(0, 10)}T${selectedTime.hour}:0${selectedTime.minute}:00.000+02:00';
    } else {
      return '${selectedDate.toString().substring(0, 10)}T${selectedTime.hour}:${selectedTime.minute}:00.000+02:00';
    }
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      locale: Locale('de', 'DE'),
      context: context,
      initialDate: _selectedDate,
      firstDate: DateTime(2019),
      lastDate: DateTime(2100),
    );
    if(picked != null && picked != _selectedDate) {
      setState(() {
        _selectedDate = picked;
      });
    }
  }

  Future<Null> _selectTime(BuildContext context, String time) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: _selectedStartTime,
    );
    if (picked != null && picked != _selectedStartTime) {
      if (time == "start") {
        setState(() {
          if (picked.hour > _selectedEndTime.hour) _selectedEndTime = picked;
          if (picked.hour == _selectedEndTime.hour && picked.minute > _selectedEndTime.minute) _selectedEndTime = picked;
          _selectedStartTime = picked;
        });
      }
      if (time == "end") {
        setState(() {
          if (_selectedStartTime.hour > picked.hour) _selectedStartTime = picked;
          if (_selectedStartTime.hour == picked.hour && _selectedStartTime.minute > picked.minute) _selectedStartTime = picked;
          _selectedEndTime = picked;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<User>(context, listen:  false);
    return Form(
      key: _reminderKey,
      child: SingleChildScrollView(
        child: Card(
          margin: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 20),
          elevation: 5,
          child: Container(
            padding: EdgeInsets.only(
                top: 10,
                left: 10,
                right: 10,
                bottom: MediaQuery.of(context).viewInsets.bottom + 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[

                TextFormField(
                  decoration: InputDecoration(labelText: "Titel"),
                  textInputAction: TextInputAction.next,
                  controller: _titleController,
                  onFieldSubmitted: (value) {
                    FocusScope.of(context).requestFocus(_descFocusNode);
                  },
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Bitte geben Sie einen Titel ein!";
                    } else {
                      return null;
                    }
                  },
                ),

                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Beschreibung',
                  ),
                  maxLines: 3,
                  focusNode: _descFocusNode,
                  controller: _descController,
                  keyboardType: TextInputType.multiline,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Bitte geben Sie eine Beschreibung ein!';
                    } else if (value.length > 180 || value.length < 10) {
                      return 'Beschreibung muss aus 10 bis 180 Zeichen bestehen!';
                    }
                    return null;
                  },
                ),

                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          RaisedButton(
                            padding: EdgeInsets.only(left: 45, right: 45),
                            onPressed: () {
                              _selectDate(context);
                            },
                            elevation: 4,
                            child: Icon(
                              Icons.calendar_today,
                              color: Color.fromRGBO(255, 255, 255, 0.9),
                            ),
                            color: Theme.of(context).primaryColor,
                          ),
                          Text(DateFormat('dd.MM.yyyy').format(_selectedDate),
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                          Center(
                            child: Builder(
                              builder: (context) => RaisedButton(
                                padding: EdgeInsets.only(left: 35, right: 35),
                                onPressed: () {
                                  _selectTime(context, "start");
                                },
                                elevation: 4,
                                child: Text(
                                  "Beginn",
                                  style: TextStyle(
                                    color: Color.fromRGBO(255, 255, 255, 0.9),
                                  ),
                                ),
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                          ),
                          Text(_selectedStartTime.format(context) + ' Uhr',
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Checkbox(
                            value: _isAllday,
                            onChanged: (value) {
                              setState(() {
                                _isAllday = value;
                              });
                            },
                          ),
                          Text("Ganztags?",
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                          Center(
                            child: Builder(
                              builder: (context) => RaisedButton(
                                padding: EdgeInsets.only(left: 40, right: 40),
                                onPressed: () {
                                  _selectTime(context, "end");
                                },
                                elevation: 4,
                                child: Text(
                                  "Ende",
                                  style: TextStyle(
                                    color: Color.fromRGBO(255, 255, 255, 0.9),
                                  ),
                                ),
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                          ),
                          Text(_selectedEndTime.format(context) + ' Uhr',
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),

                Container(
                  margin: EdgeInsets.only(top: 15),
                  child: FlatButton(
                    padding: EdgeInsets.all(10),
                    onPressed: () => _onSubmit(user.session),
                    child: Text('OK'),
                    color: Colors.green,
                    textColor: Colors.white,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0))
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
