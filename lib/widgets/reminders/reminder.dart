import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:vater_app_flutter/authentication/user.dart';
import 'package:http/http.dart' as http;

class ReminderObject {
  String id;
  String user;
  String title;
  String description;
  String startTime;
  String endTime;
  bool allDay;

  ReminderObject({
    @required this.id,
    @required this.user,
    @required this.title,
    @required this.description,
    @required this.startTime,
    @required this.endTime,
    @required this.allDay,
  });

  factory ReminderObject.fromJson(Map<String, dynamic> json) {
    return ReminderObject(
      id: json['id'] as String,
      user: json['user'] as String,
      title: json['title'] as String,
      description: json['description'] as String,
      startTime: json['startTime'] as String,
      endTime: json['endTime'] as String,
      allDay: json['allDay'] as bool,
    );
  }

  Map<String, dynamic> toJson() => 
  {
    'id': id,
    'userId': user,
    'title': title,
    'description': description,
    'startTime': startTime,
    'endTime': endTime,
    'allDay': allDay.toString()
  };
}

class Reminder with ChangeNotifier {
  List<ReminderObject> _reminders = [];

  List<ReminderObject> get reminders {
    return _reminders;
  }

  setReminders(List<ReminderObject> reminders) {
    _reminders = reminders;
  }

  Future<Map<DateTime, List>> fetchReminders(BuildContext context) async {
    final user = Provider.of<User>(context, listen: false);
    Response response;
    response = await http.get('http://212.227.10.211:8080/pp-app-server/reminder',
      headers: {
        HttpHeaders.cookieHeader: user.session,
      }
    );
    print(response.body);
    return parseReminders(response.body);
  }

  Future<num> addReminder(String title, String description, String startTime, String endTime, bool allDay, String session) async {
    Random random = Random();
    ReminderObject reminder = ReminderObject(
      id: random.nextDouble().toString(),
      user: 'x',
      title: title,
      description: description,
      startTime: startTime,
      endTime: endTime,
      allDay: allDay,
    );
    final response = await http.post('http://212.227.10.211:8080/pp-app-server/reminder',
      body: reminder.toJson(),
      headers: {
        HttpHeaders.cookieHeader: session,
      } 
    );
    return response.statusCode;
  }

  Map<DateTime, List> parseReminders(String responseBody) {
    Map<DateTime, List> mapFetch = {};
    List<ReminderObject> reminders = new List<ReminderObject>();
    var jsonParsed = jsonDecode(responseBody.toString());
    for (int i = 0; i < jsonParsed.length; i++) {
      reminders.add(new ReminderObject.fromJson(jsonParsed[i]));
    }
    reminders.sort((a, b) => a.startTime.compareTo(b.startTime));
    setReminders(reminders);
    for (int i = 0; i < reminders.length; i++) {
      if (mapFetch[DateTime.parse(reminders[i].startTime.substring(0, 10))] == null) {
        mapFetch[DateTime.parse(reminders[i].startTime.substring(0, 10))] = [reminders[i].title];
      } else {
        mapFetch[DateTime.parse(reminders[i].startTime.substring(0, 10))].addAll([reminders[i].title]);
      }
    }
    return mapFetch;
  }
}
