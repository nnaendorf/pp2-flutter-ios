import 'package:flutter/material.dart';

class User with ChangeNotifier {
  String _session = '';

  String get session {
    return _session;
  }

  void setSession(String session) {
    this._session = session;
  }
}