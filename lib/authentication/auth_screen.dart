import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:vater_app_flutter/authentication/user.dart';
import 'package:vater_app_flutter/screens/home_screen.dart';

class AuthScreen extends StatelessWidget {
  static const routeName = "/";

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Login"),
      ),
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Container(
              height: deviceSize.height*.7,
              width: deviceSize.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    flex: deviceSize.width > 600 ? 2 : 1,
                    child: AuthCard(),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class AuthCard extends StatefulWidget {
  const AuthCard({
    Key key,
  }) : super(key: key);

  @override
  _AuthCardState createState() => _AuthCardState();
}

class _AuthCardState extends State<AuthCard> {
  final GlobalKey<FormState> _authKey = GlobalKey();
  Map<String, String> _authData = {
    'email': '',
    'password': '',
  };
  var _isLoading = false;
  final _passwordController = TextEditingController();

  void _submit() {
    if (!_authKey.currentState.validate()) {
      // Invalid!
      return;
    }
    _authKey.currentState.save();
    setState(() {
      _isLoading = true;
    });
    login(_authData['email'], _authData['password']);
  }

  Future<void> login(String email, String password) async {
    String stringToEncode = email + ':' + password;
    String base64encodedAuth = base64Encode(utf8.encode(stringToEncode));
    print(base64encodedAuth);
    final response = await http.get('http://212.227.10.211:8080/pp-app-server/authentication',
      headers: {
        HttpHeaders.authorizationHeader: 'Basic ' + base64encodedAuth,
      },
    );
    print(response.statusCode);
    if (response.statusCode != 200) {
      _showAlert();
      setState(() {
        _isLoading = false;
      });
    } else {
      print(response.headers);
      int index = response.headers['set-cookie'].indexOf(';');
      String session = response.headers['set-cookie'].substring(0, index);
      Provider.of<User>(context, listen: false).setSession(session);
      setState(() {
        _isLoading = false;
      });
      Navigator.of(context).pushReplacementNamed(
        HomeScreen.routeName,
      );
    }
  }
  
  void _showAlert() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Authentifikation fehlgeschlagen!"),
          content: new Text("Etwas ist schiefgelaufen. Bitte versuchen Sie es erneut!"),
          actions: <Widget>[
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 8.0,
      child: Container(
        height: 260,
        constraints:
            BoxConstraints(minHeight: 260),
        width: deviceSize.width * 0.75,
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _authKey,
          child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(labelText: 'E-Mail'),
                keyboardType: TextInputType.emailAddress,
                validator: (value) {
                  if (value.isEmpty || !value.contains('@')) {
                    return 'E-Mail nicht vailde!';
                  }
                },
                onSaved: (value) {
                  _authData['email'] = value;
                },
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Password'),
                obscureText: true,
                controller: _passwordController,
                validator: (value) {
                  if (value.isEmpty || value.length < 5) {
                    return 'Password zu kurz!';
                  }
                },
                onSaved: (value) {
                  _authData['password'] = value;
                },
              ),
              SizedBox(
                height: 20,
              ),
              _isLoading ?
                Center(child: CircularProgressIndicator())
              :
                RaisedButton(
                  child:
                      Text("Login"),
                  onPressed: _submit,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  padding:
                      EdgeInsets.symmetric(horizontal: 30.0, vertical: 8.0),
                  color: Theme.of(context).primaryColor,
                  textColor: Theme.of(context).primaryTextTheme.button.color,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
